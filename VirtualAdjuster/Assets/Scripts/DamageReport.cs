﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReport : MonoBehaviour {

	private const float TAX_RATE = 0.0825f;

	[SerializeField] private string m_location;
	[SerializeField] private string m_description;
	[SerializeField] private float m_subtotal;

	private float m_tax;
	private float m_total;

	public string Location { 
		get{return m_location;}
		set{m_location = value;}
	}

	public string Description {
		get{return m_description;}
		set{m_description = value;}
	}

	public float SubTotal {
		get{return m_subtotal; }
		set{m_subtotal = value; }
	}

	public float Tax {
		get{
			m_tax = m_subtotal * TAX_RATE;
			return m_tax;
		}
		set{m_tax = value;}
	}

	public float Total {
		get{
			m_total = m_subtotal + Tax;
			return m_total;
		}
		set{m_total = value;}
	}
}
