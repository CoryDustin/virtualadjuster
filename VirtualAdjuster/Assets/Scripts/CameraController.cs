﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace VRStandardAssets.Utils
{
	public class CameraController : MonoBehaviour {

		[SerializeField] private VRInput m_VRInput;
		[SerializeField] private VRCameraFade m_CameraFade;
		[SerializeField] private GameObject m_exitInteriorSliderObj;
		[SerializeField] private GameObject[] m_vehicleObjList;
		[SerializeField] private VehicleDetails m_vehicleDetails;

		private int m_curVehicleIndex;
		private bool m_canTransition;
		// Use this for initialization
		void Start () {

			m_curVehicleIndex = 0;
			m_canTransition = true;

			//initDamageReportUI();

			m_VRInput.OnSwipe += SwipeHandler;

		}

		void OnDestroy()
		{
			m_VRInput.OnSwipe -= SwipeHandler;
		}
		
		// Update is called once per frame
		void Update () {

			if(Input.GetKeyUp(KeyCode.A)) 
				m_vehicleObjList[5].GetComponent<Animator>().SetTrigger ("DriveIn");
			
			if(Input.GetKeyUp(KeyCode.S)) 
				m_vehicleObjList[5].GetComponent<Animator>().SetTrigger ("DriveOut");
		}

		private void SwipeHandler(VRInput.SwipeDirection swipeDirection)
		{
			if (!GameManager.Instance.IsInspecting)
				return;

			switch (swipeDirection) 
			{
			case VRInput.SwipeDirection.LEFT: 
				m_vehicleObjList [m_curVehicleIndex].GetComponent<Animator> ().SetTrigger ("DriveOut");
				m_exitInteriorSliderObj.SetActive(false);
				m_curVehicleIndex++;
				if (m_curVehicleIndex >= m_vehicleObjList.Length) {
					m_curVehicleIndex = 0;
				}

				StartCoroutine(performTransition ());
				break;

			case VRInput.SwipeDirection.RIGHT:
				m_vehicleObjList [m_curVehicleIndex].GetComponent<Animator> ().SetTrigger ("DriveOut");
				m_exitInteriorSliderObj.SetActive(false);
				m_curVehicleIndex--;
				if (m_curVehicleIndex < 0) 
				{
					m_curVehicleIndex = m_vehicleObjList.Length - 1;
				}
				StartCoroutine(performTransition ());
				break;
			}
		}

		public IEnumerator performTransition()
		{
			if (m_canTransition) 
			{
				m_canTransition = false;

				yield return StartCoroutine(m_vehicleDetails.UpdateVehicleDetails(m_curVehicleIndex, m_vehicleObjList[m_curVehicleIndex].GetComponent<Vehicle>()));

				m_vehicleObjList [m_curVehicleIndex].GetComponent<Animator>().SetTrigger ("DriveIn");

				yield return new WaitForSeconds (0.25f);
				m_exitInteriorSliderObj.SetActive(true);

				m_canTransition = true;
			}

			yield return new WaitForEndOfFrame();
		}
	}
}
