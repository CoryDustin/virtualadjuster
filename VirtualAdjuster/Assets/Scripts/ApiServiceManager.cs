﻿using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class ApiServiceManager : MonoBehaviour {

	[SerializeField] private float latitude, longitude;
	[SerializeField] private int locationRadius = 10000;
	[SerializeField] private GameObject interstitialPanel;

	private const string PLACES_API_KEY = "AIzaSyBogYaUqqN2iYRezCC2JlBk3N0QfYS53Xc";

	public DealershipData DealershipsAvailable { get; set; }
	public VehicleData CurrentVehicle { get; set; }
	public static ApiServiceManager Instance { get; set; }

	void Awake() {
		if (Instance == null) {
			Instance = this;
		}
	}

	// Use this for initialization
	void Start() {
//		StartCoroutine(GetDealershipsFromLocation());
//		StartCoroutine (GetVehicleCost ("WUADUAFG1F7001435"));
	}
	
	public IEnumerator GetDealershipsFromLocation() {

		interstitialPanel.SetActive(true);
		yield return StartCoroutine(GetCurrentLocationCoroutine());
		if (latitude != 0 && longitude != 0) {
			string requestURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + 
				longitude + "&radius=" + locationRadius + "&type=car_dealer&key=" + PLACES_API_KEY;
		
			UnityWebRequest request = UnityWebRequest.Get (requestURL);

			yield return new WaitForSeconds (1.5f);

			yield return request.SendWebRequest ();

			if (request.isNetworkError || request.isHttpError) {
				Debug.LogError ("Request Error: " + request.error);
			} else {
				string response = request.downloadHandler.text.ToString ();
				DealershipsAvailable = JsonConvert.DeserializeObject<DealershipData>(response);
				interstitialPanel.SetActive(false);
			}
		}
	}

	public IEnumerator GetVehicleCost(string vin) {

		string requestURL = "http://ownershipcost.vinaudit.com/getownershipcost.php?vin=" + vin + "&key=VA_DEMO_KEY&state=TX";

		UnityWebRequest request = UnityWebRequest.Get (requestURL);
		yield return request.SendWebRequest ();

		if (request.isNetworkError || request.isHttpError) {
			Debug.LogError ("Vehicle CostRequest Error: " + request.error);
		} else {
			string response = request.downloadHandler.text.ToString ();
			CurrentVehicle = JsonConvert.DeserializeObject<VehicleData>(response);
		}

	}

	private IEnumerator GetCurrentLocationCoroutine() {
		#if UNITY_EDITOR
		yield return StartCoroutine(GetCurrentLocationStandalone());
		#elif UNITY_ANDROID
		yield return StartCoroutine(GetCurrentLocationMobile());
		#endif
	}

	/// <summary>
	/// Gets the current lat long for mobile devices
	/// </summary>
	/// <returns>The get current location.</returns>
	private IEnumerator GetCurrentLocationMobile() {

		if (!Input.location.isEnabledByUser) {
			Debug.LogWarning("LOCATION NOT ENABLED");
			yield break;
		}

		Input.location.Start();

		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds(1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1) {
			Debug.LogWarning("Timed out");
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed) {
			Debug.LogWarning ("Unable to determine device location");
			yield break;
		} else {
			Debug.Log ("Updating Lat: " + Input.location.lastData.latitude + " Long: " + Input.location.lastData.longitude);
			UpdateGeolocation(Input.location.lastData.latitude, Input.location.lastData.longitude);
		}
		Input.location.Stop();
	}

	/// <summary>
	/// Gets the current lat long for standalone builds
	/// </summary>
	/// <returns>The get current location.</returns>
	private IEnumerator GetCurrentLocationStandalone() {
		
		UnityWebRequest request = UnityWebRequest.Get("http://ip-api.com/json");
		yield return request.SendWebRequest ();

		if (request.isNetworkError || request.isHttpError) {
			Debug.LogError ("Request Error: " + request.error);
		} else {
			JContainer responseObj = JsonConvert.DeserializeObject(request.downloadHandler.text.ToString ()) as JContainer;
			Debug.Log ("Updating Lat: " + responseObj["lat"] + " Long: " + responseObj["lon"]);
			UpdateGeolocation(float.Parse(responseObj["lat"].ToString()), float.Parse(responseObj["lon"].ToString()));
		}
	}

	private void UpdateGeolocation(float latitude, float longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
}

/**
 * Representation objects from JSON conversion
 **/
public struct DealershipData {
	[JsonProperty("results")]
	public Dealership[] Dealerships {get; set;}
}

public struct Dealership {
	[JsonProperty("id")]
	public string Id {get; set;}
	[JsonProperty("name")]
	public string Name {get; set;}
	[JsonProperty("vicinity")]
	public string Address {get; set;}
	[JsonProperty("photos")]
	public DealershipPhotos[] Photos { get; set;}
}

public struct DealershipPhotos {
	private string[] mapLinks;
	[JsonPropertyAttribute("html_attributions")]
	public string[] MapLinks { 
		get{return mapLinks;}
		set {
			for(int i = 0; i < value.Length; i++) {
				value[i] = Regex.Split (value[0], @"href=")[1].Replace ("\"", "");
			}
			mapLinks = value;
		}
	}
}

public struct VehicleData {
	[JsonProperty("vin")]
	public string Vin { get; set;}
	[JsonProperty("vehicle")]
	public string Name { get; set;}
	[JsonProperty("depreciation_cost")]
	public int[] DepreciationCost { get; set; }
	[JsonProperty("fuel_cost")]
	public int[] FuelCost { get; set; }
	[JsonProperty("total_cost_sum")]
	public int TotalCost { get; set; }
}
