﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DealershipTile : MonoBehaviour {

	[SerializeField] private Text dealershipNameText;
	[SerializeField] private Text dealershipAddressText;

	public string DealershipName {
		get{ 
			return dealershipNameText.text;
		}
		set {
			dealershipNameText.text = value;
		}
	}

	public string DealershipAddress {
		get{ 
			return dealershipAddressText.text;
		}
		set {
			dealershipAddressText.text = value;
		}
	}
}
