﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VRStandardAssets.Utils
{
	public class GameManager : Singleton<GameManager> {

		[SerializeField] private GameObject m_playerCamera;
		[SerializeField] private GameObject m_IntroInstructionCanvas;
		[SerializeField] private GameObject m_dealershipTileCanvas;
		[SerializeField] private GameObject m_vehicleDetailCanvas;
		[SerializeField] private GameObject m_dealershipNameCanvas;
		[SerializeField] private GameObject[] m_dealershipTiles;
		[SerializeField] private Animator initialVehicleAnimator;

		[SerializeField] private SelectionSlider m_SelectionSlider;
		[SerializeField] private SelectionSlider m_viewInteriorSlider;
		[SerializeField] private SelectionSlider m_exitInteriorSlider;

		[SerializeField] private UIFader m_InstructionFader;
		[SerializeField] private VRCameraFade m_CameraFade;
		[SerializeField] private Reticle m_Reticle;  
		[SerializeField] private InputWarnings m_InputWarnings;

		[SerializeField] private Transform m_introWarpPoint;
		[SerializeField] private Transform m_interiorWarpPoint;

		[SerializeField] private Text m_dealershipSelectedName;

		private bool m_isInspecting;
		private bool m_hasSelectedDealer;

		// Use this for initialization
		private IEnumerator Start () {
			// Start the intro phase and wait for user interation
			yield return StartCoroutine(StartPhase());
			m_viewInteriorSlider.OnBarFilled += OnInteriorBarFilled;
			m_exitInteriorSlider.OnBarFilled += OnExitInteriorBarFilled;
		}

		private void Update() {

			if (Input.GetKeyDown (KeyCode.Q)) {
				m_IntroInstructionCanvas.SetActive(false);
				StartCoroutine(ApiServiceManager.Instance.GetDealershipsFromLocation());
			}

			if(Input.GetKeyDown(KeyCode.W))
				GenerateDealershipTiles();

			if (Input.GetKeyDown (KeyCode.A)) 
				OnDealershipBarFilled(m_dealershipTiles[0].GetComponent<SelectionSlider>().gameObject);

			if (Input.GetKeyDown (KeyCode.S))
				StartCoroutine(Test ());
		}

		private IEnumerator Test() {
			yield return StartCoroutine (m_CameraFade.BeginFadeOut(0.25f, false));

			m_playerCamera.transform.position = m_introWarpPoint.position;
			m_playerCamera.transform.rotation = m_introWarpPoint.rotation;
			m_vehicleDetailCanvas.SetActive(true);
			//			m_Reticle.Hide();

			yield return StartCoroutine (m_CameraFade.BeginFadeIn(0.25f, false));

			initialVehicleAnimator.SetTrigger ("DriveIn");
			m_isInspecting = true;
		}

		private IEnumerator StartPhase()
		{
			m_isInspecting = false;

			yield return StartCoroutine (m_InstructionFader.InteruptAndFadeIn());
			// Turn the reticle on and make it flat to the screen so it can be used with the selection slider.
			m_Reticle.Show();
			m_Reticle.UseNormal = false;

			// The user should hold Fire1 at this point so warn against tapping.
			m_InputWarnings.TurnOnDoubleTapWarnings ();

			// Wait for the selection bar to fill indicating the user has read the instructions.
			yield return StartCoroutine (m_SelectionSlider.WaitForBarToFill());

			// Turn off the double tap warnings since the user will need to use double tap to move the character.
			//m_InputWarnings.TurnOffDoubleTapWarnings();

			m_IntroInstructionCanvas.SetActive(false);
			yield return StartCoroutine(ApiServiceManager.Instance.GetDealershipsFromLocation());

			// Wait for selection of dealership
			GenerateDealershipTiles();

			// Wait for the instructions to fade out.
			while (!m_hasSelectedDealer)
				yield return new WaitForEndOfFrame();

			//yield return StartCoroutine (m_InstructionFader.InteruptAndFadeOut());

			yield return StartCoroutine (m_CameraFade.BeginFadeOut(0.25f, false));

			m_playerCamera.transform.position = m_introWarpPoint.position;
			m_playerCamera.transform.rotation = m_introWarpPoint.rotation;

			m_vehicleDetailCanvas.SetActive(true);
			m_dealershipNameCanvas.SetActive (true);
			m_dealershipTileCanvas.SetActive(false);

			yield return StartCoroutine (m_CameraFade.BeginFadeIn(0.25f, false));

			initialVehicleAnimator.SetTrigger ("DriveIn");
			m_isInspecting = true;
		}

		public void GenerateDealershipTiles() {
			for(int i = 0; i < m_dealershipTiles.Length; i++) {
				m_dealershipTiles[i].GetComponent<DealershipTile>().DealershipName = ApiServiceManager.Instance.DealershipsAvailable.Dealerships[i].Name;
				m_dealershipTiles[i].GetComponent<DealershipTile>().DealershipAddress = ApiServiceManager.Instance.DealershipsAvailable.Dealerships[i].Address;
				m_dealershipTiles [i].GetComponent<SelectionSlider>().OnDealershipBarFilled += OnDealershipBarFilled;
				m_dealershipTiles[i].SetActive (true);
			}
			m_dealershipTileCanvas.SetActive(true);
		}

		private void OnDealershipBarFilled(GameObject other) {
			m_dealershipSelectedName.text = other.GetComponent<DealershipTile>().DealershipName;
			m_hasSelectedDealer = true;
		}

		private void OnInteriorBarFilled() {
			StartCoroutine (OnInteriorBarFilledCoroutine());
		}

		private void OnExitInteriorBarFilled() {
			StartCoroutine (OnExitInteriorBarFilledCoroutine());
		}

		private IEnumerator OnInteriorBarFilledCoroutine() {
			m_isInspecting = false;

			yield return StartCoroutine (m_CameraFade.BeginFadeOut(0.25f, false));

			m_playerCamera.transform.position = m_interiorWarpPoint.position;
			m_playerCamera.transform.rotation = m_interiorWarpPoint.rotation;

			yield return StartCoroutine (m_CameraFade.BeginFadeIn(0.25f, false));
		}

		private IEnumerator OnExitInteriorBarFilledCoroutine() {
			m_isInspecting = false;

			yield return StartCoroutine (m_CameraFade.BeginFadeOut(0.25f, false));

			m_playerCamera.transform.position = m_introWarpPoint.position;
			m_playerCamera.transform.rotation = m_introWarpPoint.rotation;
			m_exitInteriorSlider.gameObject.SetActive(false);
		
			yield return StartCoroutine (m_CameraFade.BeginFadeIn(0.25f, false));
			m_isInspecting = true;
		}
			
		private IEnumerator TestButtonClick() {
			m_IntroInstructionCanvas.SetActive(false);
			yield return StartCoroutine(ApiServiceManager.Instance.GetDealershipsFromLocation());

			// Wait for selection of dealership
			GenerateDealershipTiles();
			m_dealershipTileCanvas.SetActive(true);
		}

		public bool IsInspecting
		{
			get{ return m_isInspecting; }
			set{ m_isInspecting = value; }
		}
			
	}

}