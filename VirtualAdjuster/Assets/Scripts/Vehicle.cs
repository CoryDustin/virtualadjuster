﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : MonoBehaviour {

	[SerializeField] private string color;
	[SerializeField] private string mileage;
	[SerializeField] private string msrp;
	[SerializeField] private string dealerPrice;
	[SerializeField] private string estimatedPayment;
	[SerializeField] private string apr;

	public string Color {
		get { return color; }
	}

	public string Mileage {
		get { return mileage; }
	}

	public string MSRP {
		get { return msrp; }
	}

	public string DealerPrice {
		get { return dealerPrice; }
	}

	public string EstimatedPayment {
		get { return estimatedPayment; }
	}

	public string APR {
		get { return apr; }
	}

}
