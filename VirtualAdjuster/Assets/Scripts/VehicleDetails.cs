﻿using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VRStandardAssets.Utils
{
	public class VehicleDetails : MonoBehaviour {

		[SerializeField] private Text nameText;
		[SerializeField] private Text vinText;
		[SerializeField] private Text colorText;
		[SerializeField] private Text mileageText;
		[SerializeField] private Text MSRPText;
		[SerializeField] private Text dealerPriceText;
		[SerializeField] private Text depreciationText;
		[SerializeField] private Text fuelCostText;
		[SerializeField] private Text estimatedPaymentText;
		[SerializeField] private Text APRText;
		[SerializeField] private SelectionSlider applySlider;

		private string color;
		private string mileage;
		private string msrp;
		private string dealerPrice;
		private string estimatedPayment;
		private string apr;

		// Use this for initialization
		void Start () {
			StartCoroutine (UpdateVehicleDetails(0, null));
			applySlider.OnBarFilled += OnApplyBarFilled;
		}

		public IEnumerator UpdateVehicleDetails(int index, Vehicle vehicleDetails) {
			yield return ApiServiceManager.Instance.GetVehicleCost(VinList[index]);

			nameText.text = "<b>" + ApiServiceManager.Instance.CurrentVehicle.Name + "</b>";
			vinText.text = "<b>" + VinList[index] + "</b>";
			depreciationText.text = "<b>" + ApiServiceManager.Instance.CurrentVehicle.DepreciationCost[0].ToString("C", CultureInfo.CurrentCulture) + "</b>";
			fuelCostText.text = "<b>" + ApiServiceManager.Instance.CurrentVehicle.FuelCost[0].ToString("C", CultureInfo.CurrentCulture) + "</b>";

			if (vehicleDetails != null) {
				colorText.text = "<b>" + vehicleDetails.Color + "</b>";
				mileageText.text = "<b>" + vehicleDetails.Mileage + "</b>";
				MSRPText.text = "<b>" + vehicleDetails.MSRP + "</b>";
				dealerPriceText.text = "<b>" + vehicleDetails.DealerPrice + "</b>";
				estimatedPaymentText.text = "<b>" + vehicleDetails.EstimatedPayment + "</b>";
				APRText.text = "<b>" + vehicleDetails.APR + "</b>";
			}
		}

		public void OnApplyBarFilled() {}

		public string[] VinList {
			get{ 
				string[] vins = { "WAUKMAF46JN006724",
								  "WAUGNAF47HN061096",
								  "WAUFFAFL9GN001237",
								  "WAUAFAFL4FN021775",
							      "WAUFFBFL6FN041196",
								  "WAUFFAFL8FN003186"};
				return vins;
			}
		}

		public string Color {
			get{
				return color;
			} 
			set{
				color = value;
				colorText.text = color;
			}
		}

		public string Mileage {
			get{
				return mileage;
			} 
			set{
				mileage = value;
				mileageText.text = mileage;
			}
		}

		public string MSRP {
			get{
				return msrp;
			} 
			set{
				msrp = value;
				MSRPText.text = msrp;
			}
		}

		public string DealerPrice {
			get{
				return dealerPrice;
			} 
			set{
				dealerPrice = value;
				dealerPriceText.text = dealerPrice;
			}
		}

		public string EstimatedPayment {
			get{
				return estimatedPayment;
			} 
			set{
				estimatedPayment = value;
				estimatedPaymentText.text = estimatedPayment;
			}
		}

		public string APR {
			get{
				return apr;
			} 
			set{
				apr = value;
				APRText.text = apr;
			}
		}

	}
}